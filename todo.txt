REST API endpoint

POST - https://XXXXXX.execute-api.us-east-1.amazonaws.com/dev/school
PUT - https://XXXXX.execute-api.us-east-1.amazonaws.com/dev/school/{id}
DELETE - https://XXXXX.execute-api.us-east-1.amazonaws.com/dev/school/{id}
GET - https://XXXXX.execute-api.us-east-1.amazonaws.com/dev/school/{id}
GET - https://XXXXX.execute-api.us-east-1.amazonaws.com/dev/schools

todo:

1) understand how the existing tests work
2) in case 2, assert that the name returned is the same as the name we inserted in case 1
3) in case 6, assert that the name returned is not the same as the name returned in case 4
4) in case 6, assert that the name returned is the same as the name we updated in case 5
5) in case 6, assert that the modifiedDate returned is more recent than the modifiedDate in case 4
6) add a case 7, to assert that the error message "name is required and can not be null" is returned when try to insert with invalid or missing 'name'.
7) add a case 8, to assert that the error message "name is required and can not be null" is returned when try to update with invalid or missing 'name'.
8) in case 12, assert that the object deleted in case 11 is no longer in the result
9) in case 12, assert that the object not deleted in case 11 is still in the result  
10) in case 14, assert that the object deleted in case 11 and case 13 are no longer in the result
