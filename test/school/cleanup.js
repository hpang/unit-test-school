var fs = require('fs');
var chai = require("chai");
var expect = chai.expect;
var assert = chai.assert;
var should = chai.should();
var request = require("request");
var supertest = require("supertest");
var lambdaUrl = require("../../lambdaUrl");
console.log('lambda url => ' + lambdaUrl);
var api = supertest(lambdaUrl);
var updateObjectId;
var deleteObjectId;
var collectionName = "school";
var resultFilePath = 'test/' + collectionName + '/testResult.txt';
var array = fs.readFileSync(resultFilePath).toString().split("\n");
if (array.length > 0) {
	updateObjectId = array[0];
}
if (array.length > 1) {
	deleteObjectId = array[1];
}
describe("Cleanup for  " + collectionName + " crud", function() {
	describe("case 11, delete " + collectionName + " 1", function() {
		it("delete " + collectionName + " 1", (done) => {
			api.delete("/" + collectionName + "/" + deleteObjectId).expect(200).end((err, res) => {
				// res.body.should.be.an('array');
				// res.body.should.have.lengthOf(1);
				console.log('json response: ' + JSON.stringify(res.body));
				done();
			});
		});
	});
	describe("case 12, get all " + collectionName + "s", function() {
		it("get all " + collectionName + "s", (done) => {
			api.get("/" + collectionName + "s").expect(200).end((err, res) => {
				res.body.should.be.a('object');
				res.body.should.have.property('result');
				res.body.result.should.be.a('array');
				console.log('json response: ' + JSON.stringify(res.body));
				done();
			});
		});
	});
	describe("case 13, delete " + collectionName + " 2", function() {
		it("delete " + collectionName + " 2", (done) => {
			api.delete("/" + collectionName + "/" + updateObjectId).expect(200).end((err, res) => {
				// res.body.should.be.an('array');
				// res.body.should.have.lengthOf(1);
				console.log('json response: ' + JSON.stringify(res.body));
				done();
			});
		});
	});
	describe("case 14, get all " + collectionName + "s", function() {
		it("get all " + collectionName + "s", (done) => {
			api.get("/" + collectionName + "s").expect(200).end((err, res) => {
				res.body.should.be.a('object');
				res.body.should.have.property('result');
				res.body.result.should.be.a('array');
				console.log('json response: ' + JSON.stringify(res.body));
				done();
			});
		});
	});
});
