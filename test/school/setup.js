var fs = require('fs');
var chai = require("chai");
var expect = chai.expect;
var assert = chai.assert;
var should = chai.should();
var request = require("request");
var supertest = require("supertest");
var lambdaUrl = require("../../lambdaUrl");
console.log('lambda url => ' + lambdaUrl);
var api = supertest(lambdaUrl);
var collectionName = "school";
var resultFilePath = 'test/' + collectionName + '/testResult.txt';
if (fs.existsSync(resultFilePath)) {
	fs.unlinkSync(resultFilePath);
}