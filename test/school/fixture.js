var fs = require('fs');
var chai = require("chai");
var expect = chai.expect;
var assert = chai.assert;
var should = chai.should();
var request = require("request");
var supertest = require("supertest");
var lambdaUrl = require("../../lambdaUrl");
console.log('lambda url => ' + lambdaUrl);
var api = supertest(lambdaUrl);
var updateObjectId;
var deleteObjectId;
var collectionName = "school";
var resultFilePath = 'test/' + collectionName + '/testResult.txt';
describe("test cases for " + collectionName + " crud", function() {
	describe("case 1, create new " + collectionName + " 1", function() {
		it("post a new " + collectionName + " 1", (done) => {
			var json = {
				name: "test " + collectionName + " 1"
			}
			api.post("/" + collectionName).send(json).expect(200).end((err, res) => {
				console.log('json response: ' + JSON.stringify(res.body));
				res.body.should.be.a('object');
				res.body.should.have.property('result');
				res.body.result.should.be.a('array');
				deleteObjectId = res.body.result[0].id;
				var line= deleteObjectId + '\r\n';
				fs.appendFileSync(resultFilePath, line);
				done();
			});
		});
	});
	describe("case 2, get saved " + collectionName + " 1", function() {
		it("get saved " + collectionName + " 1", (done) => {
			api.get("/" + collectionName + "/" + deleteObjectId).expect(200).end((err, res) => {
				console.log('json response: ' + JSON.stringify(res.body));
				res.body.should.be.a('object');
				res.body.should.have.property('result');
				res.body.result.should.be.a('array');
				res.body.result.should.have.lengthOf(1);
				done();
			});
		});
	});
	describe("case 3, create new " + collectionName + " 2", function() {
		it("post a new " + collectionName + " 2", (done) => {
			var json = {
				name: "test " + collectionName + " 2",
				status: 1
			}
			api.post("/" + collectionName).send(json).expect(200).end((err, res) => {
				console.log('json response: ' + JSON.stringify(res.body));
				res.body.should.be.a('object');
				res.body.should.have.property('result');
				res.body.result.should.be.a('array');
				updateObjectId = res.body.result[0].id;
				var line= updateObjectId + '\r\n';
				fs.appendFileSync(resultFilePath, line);
				done();
			});
		});
	});
	describe("case 4, get saved " + collectionName + " 2", function() {
		it("get saved " + collectionName + " 2", (done) => {
			api.get("/" + collectionName + "/" + updateObjectId).expect(200).end((err, res) => {
				console.log('json response: ' + JSON.stringify(res.body));
				res.body.should.be.a('object');
				res.body.should.have.property('result');
				res.body.result.should.be.a('array');
				res.body.result.should.have.lengthOf(1);
				done();
			});
		});
	});
	describe("case 5, update new " + collectionName + " 2", function() {
		it("update new " + collectionName + " 2", (done) => {
			var json = {
				name: "updated " + collectionName + " 2",
				status: 1
			}
			api.put("/" + collectionName + "/" + updateObjectId).send(json).expect(200).end((err, res) => {
				//res.body.should.be.a('string');
				console.log('json response: ' + JSON.stringify(res.body));
				done();
			});
		});
	});
	describe("case 6, get updated " + collectionName + " 2", function() {
		it("get updated " + collectionName + " 2", (done) => {
			api.get("/" + collectionName + "/" + updateObjectId).expect(200).end((err, res) => {
				console.log('json response: ' + JSON.stringify(res.body));
				res.body.should.be.a('object');
				res.body.should.have.property('result');
				res.body.result.should.be.a('array');
				res.body.result.should.have.lengthOf(1);
				done();
			});
		});
	});
});
