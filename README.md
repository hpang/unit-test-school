
steps to setup and run test scripts

1) install npm dependencies

npm install

2) update lambdaUrl.js with actual url

3) run setup script

npm test test/school/setup

4) run unit test scripts

npm test test/school/fixture

5) run clean up script

npm test test/school/cleanup
